﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Распознавание_образов___5 {
	public partial class Form1 : Form {
		int PictSize = 400;
		Graphics Graph;
		Random R = new Random();
		int TotalCount = 0;
		int PageCnt = 0;
		int Dimens = 0;
		float[,] TotPoints;
		float[] Prob;		//вероятность класса
		float[,] Mat;	//мат.ожидания
		float[, ,] Corr;	//матрица корелляции
		float[,] NewMat;
		float[, ,] NewCorr;
		float[] NewProb;
		List<float[,]> PointsList;
		Brush[] ClassColor;
		Brush[] BkColor;

		public Form1() {
			InitializeComponent();
			CreateTabs();
			pictureBox1.Image = new Bitmap(PictSize, PictSize);
			Graph = Graphics.FromImage(pictureBox1.Image);
			comboBox1.Items.Add("  введённых значений"); 
			comboBox1.Items.Add("  оценённых значений");
			comboBox1.SelectedIndex = 1;
		}
		void CreateTabs() {
			tabControl1.TabPages.Clear();	//очистка
			for(int Page = 0; Page < numClassCount.Value; Page++) {
				TabPage NewPage = new TabPage(		//создание новой панели
					"Класс " + Convert.ToString(Page + 1));	//надпись
				NewPage.AutoScroll = true;
				NewPage.BackColor = Color.White;

				Label ProbLab = new Label();		//надпись "Вероятность"
				ProbLab.Location = new Point(0, 8);//положение
				ProbLab.Text = "Вероятность:";		//надпись
				ProbLab.AutoSize = true;			//авто размер
				NewPage.Controls.Add(ProbLab);		//добавление на панель

				TextBox ProbText = new TextBox();	//текстовое поле вероятности
				ProbText.Location = new Point(130, 5);	//положение
				ProbText.Text = Convert.ToString(1 / (float) numClassCount.Value);	//надпись
				ProbText.Size = new Size(50, 20);	//размер
				ProbText.Name = "Prob";				//имя - чтобы можно было найти
				NewPage.Controls.Add(ProbText);		//добавление на панель

				Label MatLab = new Label();			//надпись "Мат ожидания"
				MatLab.Location = new Point(0, 43);	//положение
				MatLab.Text = "Мат. ожидания:";		//надпись
				MatLab.AutoSize = true;				//авто размер
				NewPage.Controls.Add(MatLab);		//добавление на панель

				for(int Pos = 0; Pos < numVecSize.Value; Pos++) {
					TextBox MatText = new TextBox();	//текстовое поле мат. ожидания
					MatText.Location = new Point(130 + 50 * Pos, 40);	//положение
					MatText.Text = Convert.ToString(R.Next(200) + 100); //надпись
					MatText.Size = new Size(50, 20);	//размер
					MatText.Name = "Math_" + Pos.ToString();	//имя - чтобы можно было найти
					NewPage.Controls.Add(MatText);		//добавление на панель
				}

				Label CorLab = new Label();			//надпись "Корреляция"
				CorLab.Location = new Point(0, 79);	//положение
				CorLab.Text = "Корреляция";			//надпись
				CorLab.AutoSize = true;				//авто размер
				NewPage.Controls.Add(CorLab);		//добавление на панель

				for(int Pos = 0; Pos < numVecSize.Value; Pos++)
					for(int ToPos = 0; ToPos <= Pos; ToPos++) {
						TextBox CorText = new TextBox();	//текстовое поле корреляции
						CorText.Location = new Point(130 + 50 * ToPos, 75 + 25 * Pos);	//положение
						if(Pos == ToPos)
							CorText.Text = "50";				//надпись
						else
							CorText.Text = "0";				//надпись
						CorText.Size = new Size(50, 20);	//размер
						CorText.Name = "Corr_" + Pos.ToString()	//имя - чтобы можно было найти
							+ "_" + ToPos.ToString();
						NewPage.Controls.Add(CorText);		//добавление на панель
					}

				tabControl1.TabPages.Add(NewPage);	//добавление панели на форму
			}
			CreateDisableTabs();
		}
		//генерация вкладок
		void CreateDisableTabs() {
			tabControl2.TabPages.Clear();	//очистка
			for(int Page = 0; Page < numClassCount.Value; Page++) {
				TabPage NewPage = new TabPage(		//создание новой панели
					"Класс " + Convert.ToString(Page + 1));	//надпись
				NewPage.AutoScroll = true;
				NewPage.BackColor = Color.White;

				Label ProbLab = new Label();		//надпись "Вероятность"
				ProbLab.Location = new Point(0, 8);//положение
				ProbLab.Text = "Вероятность:";		//надпись
				ProbLab.AutoSize = true;			//авто размер
				NewPage.Controls.Add(ProbLab);		//добавление на панель

				TextBox ProbText = new TextBox();	//текстовое поле вероятности
				ProbText.Location = new Point(130, 5);	//положение
				ProbText.Text = ""; //надпись
				ProbText.Size = new Size(50, 20);	//размер
				ProbText.Name = "Prob";				//имя - чтобы можно было найти
				ProbText.Enabled = false;
				NewPage.Controls.Add(ProbText);		//добавление на панель

				Label MatLab = new Label();			//надпись "Мат ожидания"
				MatLab.Location = new Point(0, 43);	//положение
				MatLab.Text = "Мат. ожидания:";		//надпись
				MatLab.AutoSize = true;				//авто размер
				NewPage.Controls.Add(MatLab);		//добавление на панель

				for(int Pos = 0; Pos < numVecSize.Value; Pos++) {
					TextBox MatText = new TextBox();	//текстовое поле мат. ожидания
					MatText.Location = new Point(130 + 50 * Pos, 40);	//положение
					MatText.Text = "";					//надпись
					MatText.Size = new Size(50, 20);	//размер
					MatText.Name = "Math_" + Pos.ToString();	//имя - чтобы можно было найти
					MatText.Enabled = false;
					NewPage.Controls.Add(MatText);		//добавление на панель
				}

				Label CorLab = new Label();			//надпись "Корреляция"
				CorLab.Location = new Point(0, 79);	//положение
				CorLab.Text = "Корреляция";			//надпись
				CorLab.AutoSize = true;				//авто размер
				NewPage.Controls.Add(CorLab);		//добавление на панель

				for(int Pos = 0; Pos < numVecSize.Value; Pos++)
					for(int ToPos = 0; ToPos <= Pos; ToPos++) {
						TextBox CorText = new TextBox();	//текстовое поле корреляции
						CorText.Location = new Point(130 + 50 * ToPos, 75 + 25 * Pos);	//положение
						CorText.Text = "";					//надпись
						CorText.Size = new Size(50, 20);	//размер
						CorText.Name = "Corr_" + Pos.ToString()	//имя - чтобы можно было найти
							+ "_" + ToPos.ToString();
						CorText.Enabled = false;
						NewPage.Controls.Add(CorText);		//добавление на панель
					}

				tabControl2.TabPages.Add(NewPage);	//добавление панели на форму
			}
		}

		private void numClassCount_ValueChanged(object sender, EventArgs e) {
				CreateTabs();
		}
		void GenPoints(int Page,out float[,] Points,out int Count) {

			TabPage CurPage = tabControl1.TabPages[Page];
			Prob[Page] = Convert.ToSingle(	//вероятность - получение из нужного поля
				CurPage.Controls["Prob"].Text.Replace('.', ','));	//замена точки на запятую

			for(int Pos = 0; Pos < Dimens; Pos++)
				Mat[Page, Pos] = Convert.ToSingle(	//Мат.ожидание - получение из нужного поля
				CurPage.Controls["Math_" + Pos.ToString()]
				.Text.Replace('.', ','));	//замена точки на запятую

			for(int Pos = 0; Pos < Dimens; Pos++)	//корелляция
				for(int ToPos = 0; ToPos <= Pos; ToPos++)
					Corr[Page, Pos, ToPos] = Convert.ToSingle(	//Мат.ожидание - получение из нужного поля
					CurPage.Controls["Corr_" + Pos.ToString() + "_" + ToPos.ToString()]
					.Text.Replace('.', ','));	//замена точки на запятую
			for(int Pos = 0; Pos < Dimens; Pos++)
				for(int ToPos = Pos + 1; ToPos < Dimens; ToPos++)
					Corr[Page, Pos, ToPos] = Corr[Page, ToPos, Pos];

			// генерация выборки
			float[,] A = new float[Dimens, Dimens];
			A[0, 0] = Corr[Page, 0, 0];
			for(int i = 0; i < Dimens; i++) {
				A[i, 0] = Corr[Page, i, 0];
				for(int j = 1; j < i; j++) {
					float sum = 0;
					for(int m = 0; m < i; m++)
						sum += A[i, m] * A[j, m];
					A[i, j] = (Corr[Page, i, j] - sum) / A[j, j];
				}
				float ssum = 0;
				for(int m = 0; m < i; m++)
					ssum += A[i, m] * A[i, m];
				A[i, i] = (float) Math.Sqrt(1 - ssum);

			}

			Count = (int) (TotalCount * Prob[Page]);
			Points = new float[Count, Dimens];
			for(int i = 0; i < Count; i++) {
				float[] X = new float[Dimens];	//генерация некореллированной выборки
				for(int j = 0; j < Dimens; j++) {
					float sum = 0;
					for(int k = 0; k < 12; k++)
						sum += (float) R.NextDouble();
					X[j] = sum - 6;
				}
				for(int j = 0; j < Dimens; j++) {
					Points[i, j] = 0;
					for(int k = 0; k <= j; k++)
						Points[i, j] += X[k] * A[j, k];
					Points[i, j] = Corr[Page, j, j] * Points[i, j] + Mat[Page, j];

				}
				Graph.FillEllipse(ClassColor[Page], Points[i, 0] - 1, PictSize - Points[i, 1] - 1, 3, 3);
			}

		}
		void Analyse(int Page, float[,] Points, int Count) {
			NewProb[Page] = (float) Count / TotalCount;
			for(int j = 0; j < Dimens; j++) {
				NewMat[Page, j] = 0;
				for(int i = 0; i < Count; i++)
					NewMat[Page, j] += Points[i, j];
				NewMat[Page, j] /= Count;
			}
			for(int i = 0; i < Dimens; i++) {
				float sum = 0;
				for(int j = 0; j < Count; j++)
					sum += Points[j, i] * Points[j, i];
				NewCorr[Page, i, i] = (float) Math.Sqrt(sum / Count - NewMat[Page, i] * NewMat[Page, i]);
				for(int j = 0; j < i; j++) {
					sum = 0;
					for(int k = 0; k < Count; k++)
						sum += (Points[k, i] - NewMat[Page, i]) * (Points[k, j] - NewMat[Page, j]);
					NewCorr[Page, i, j] = sum / Count / NewCorr[Page, i, i] / NewCorr[Page, j, j];
				}

			}
			TabPage CurPage = tabControl2.TabPages[Page];
			CurPage.Controls["Prob"].Text = NewProb[Page].ToString();

			for(int Pos = 0; Pos < Dimens; Pos++)
				CurPage.Controls["Math_" + Pos.ToString()].Text = NewMat[Page, Pos].ToString();

			for(int Pos = 0; Pos < Dimens; Pos++)	//корелляция
				for(int ToPos = 0; ToPos <= Pos; ToPos++)
					CurPage.Controls["Corr_" + Pos.ToString() + "_" + ToPos.ToString()]
					.Text = NewCorr[Page, Pos, ToPos].ToString();
		}
		private void button1_Click(object sender, EventArgs e) {
			Graph.Clear(Color.White);
			TotalCount = (int) numSize.Value;	//общее число точек
			Dimens = (int) numVecSize.Value;	//размерность вектора
			PageCnt = (int) numClassCount.Value;
			Prob = new float[PageCnt];
			Mat = new float[PageCnt, Dimens];
			Corr = new float[PageCnt, Dimens, Dimens];
			NewProb = new float[PageCnt];
			NewMat = new float[PageCnt, Dimens];
			NewCorr = new float[PageCnt, Dimens, Dimens];
			TotPoints = new float[TotalCount, Dimens];
			ClassColor = new Brush[PageCnt];
			BkColor = new Brush[PageCnt];
			int PointPos = 0;
			PointsList = new List<float[,]>();
			for(int Page = 0; Page < PageCnt; Page++) {
				int r = R.Next(255), g = R.Next(255), b = R.Next(255);
				ClassColor[Page] = new SolidBrush(Color.FromArgb(r, g, b));
				BkColor[Page] = new SolidBrush(Color.FromArgb(r / 2, g / 2, b / 2));
				int Count;
				float[,] Points;
				GenPoints(Page, out Points, out Count);
				Analyse(Page, Points, Count);
				for(int i = 0; i < Count; i++) {
					for(int j = 0; j < Dimens; j++)
						TotPoints[PointPos, j] = Points[i, j];
					PointPos++;
				}
				PointsList.Add(Points);
			}
			pictureBox1.Refresh();
			button2.Enabled = true;
			button3.Enabled = true;
		}
		float Dist(float[] arr1, int ind2, int max) {
			float d = 0;
			for(int i = 0; i < max; i++)
				if(comboBox1.SelectedIndex==1)
					d += (arr1[i] - NewMat[ind2, i]) * (arr1[i] - NewMat[ind2, i]);
				else
					d += (arr1[i] - Mat[ind2, i]) * (arr1[i] - Mat[ind2, i]);
			return d;
		}
		float DistBaues(float[] Pnt, int ind2) {
			if(comboBox1.SelectedIndex == 1) {
				float dmx = Pnt[0] - NewMat[ind2, 0];
				float dmy = Pnt[1] - NewMat[ind2, 1];
				float dxdy = NewCorr[ind2, 0, 0] * NewCorr[ind2, 1, 1];
				float cov = NewCorr[ind2, 0, 1] * (float) Math.Sqrt(dxdy);
				float det = dxdy - (float) Math.Pow(NewCorr[ind2, 1, 0], 2);
				float dist = (dmx * dmx * NewCorr[ind2, 1, 1] + dmy * dmy * NewCorr[ind2, 0, 0]
					- 2 * cov * dmx * dmy) / (-det) + 2 * (float) Math.Log(NewProb[ind2]) -
					(float) Math.Log(det);
				return dist;
			}
			else {
				float dmx = Pnt[0] - Mat[ind2, 0];
				float dmy = Pnt[1] - Mat[ind2, 1];
				float dxdy = Corr[ind2, 0, 0] * Corr[ind2, 1, 1];
				float cov = Corr[ind2, 0, 1] * (float) Math.Sqrt(dxdy);
				float det = dxdy - (float) Math.Pow(Corr[ind2, 1, 0], 2);
				float dist = (dmx * dmx * Corr[ind2, 1, 1] + dmy * dmy * Corr[ind2, 0, 0]
					- 2 * cov * dmx * dmy) / (-det) + 2 * (float) Math.Log(Prob[ind2]) -
					(float) Math.Log(det);
				return dist;
			}
		}
		const float RAD = 100;
		private void button2_Click(object sender, EventArgs e) {
			int iii = 0;
			Graph.Clear(Color.White);
			List<float[]>[] Sorted = new List<float[]>[PageCnt];
			for(int i = 0; i < PageCnt; i++)
				Sorted[i] = new List<float[]>();

			for(int i = 0; i < TotalCount; i++) {
				float[] Pnt = new float[Dimens];
				for(int j = 0; j < Dimens; j++)
					Pnt[j] = TotPoints[i, j];
				int Nearest = 0;
				float min = Dist(Pnt, 0, Dimens);
				for(int cl = 0; cl < PageCnt; cl++) {
					float d = Dist(Pnt, cl, Dimens);
					if(d < min) {
						Nearest = cl;
						min = d;
					}
				}
				Sorted[Nearest].Add(Pnt);
			}

			float[] GP = new float[2];
			for(GP[0] = 0; GP[0] < PictSize; GP[0]++)
				for(GP[1] = 0; GP[1] < PictSize; GP[1]++) {
					int Nearest = 0;
					float min = Dist(GP, 0, 2);
					for(int cl = 0; cl < PageCnt; cl++) {
						float d = Dist(GP, cl, 2);
						if(d < min) {
							Nearest = cl;
							min = d;
						}
					}
					iii++;
					Graph.FillEllipse(BkColor[Nearest], GP[0], PictSize - GP[1], 2, 2);
				}

			pictureBox1.Refresh();
			for(int i = 0; i < PageCnt; i++) {
				float[,] P = PointsList[i];
				for(int j = 0; j < P.GetLength(0); j++){
					Graph.FillEllipse(ClassColor[i], P[j, 0] - 1, PictSize - P[j, 1] - 1, 3, 3);
					iii++;
				}
			}

			pictureBox1.Refresh();
		}
		private void button3_Click(object sender, EventArgs e) {

			Graph.Clear(Color.White);
			List<float[]>[] Sorted = new List<float[]>[PageCnt];
			for(int i = 0; i < PageCnt; i++)
				Sorted[i] = new List<float[]>();

			for(int i = 0; i < TotalCount; i++) {
				float[] Pnt = new float[Dimens];
				for(int j = 0; j < Dimens; j++)
					Pnt[j] = TotPoints[i, j];
				int Nearest = 0;
				float min = DistBaues(Pnt, 0);
				for(int cl = 0; cl < PageCnt; cl++) {
					float d = DistBaues(Pnt, cl);
					if(d >= min) {
						Nearest = cl;
						min = d;
					}
				}
				Sorted[Nearest].Add(Pnt);
			}

			float[] GP = new float[2];
			for(GP[0] = 0; GP[0] < PictSize; GP[0]++)
				for(GP[1] = 0; GP[1] < PictSize; GP[1]++) {
					int Nearest = 0;
					float min = DistBaues(GP, 0);
					for(int cl = 0; cl < PageCnt; cl++) {
						float d = DistBaues(GP, cl);
						if(d >= min) {
							Nearest = cl;
							min = d;
						}
					}
					Graph.FillEllipse(BkColor[Nearest], GP[0], PictSize - GP[1], 3, 3);
				}

			pictureBox1.Refresh();
			for(int i = 0; i < PageCnt; i++) {
				float[,] P = PointsList[i];
				for(int j = 0; j < P.GetLength(0); j++)
					Graph.FillEllipse(ClassColor[i], P[j, 0] - 1, PictSize - P[j, 1] - 1, 3, 3);
			}
			pictureBox1.Refresh();
		}
	}
}