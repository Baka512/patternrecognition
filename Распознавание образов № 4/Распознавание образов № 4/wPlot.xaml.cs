﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace Распознавание_образов___4 {
	/// <summary>
	/// Логика взаимодействия для wPlot.xaml
	/// </summary>
	public partial class wPlot : UserControl {
		private List<List<Point>> pLines;
		//List<Point> savePointsList;
		private double  MaxY = 0, MinY = 0,
						MaxX = 0, MinX = 0,
						majorStepX = 1.0, minorStepX = 1,
						majorStepY = 1.0, minorStepY = 0.1;
		public wPlot(){
			InitializeComponent();
		}

		public void setData(List<List<Point>> PL) {
			pLines = PL;

			foreach (var T in pLines)
				foreach (var P in T){
					MaxY = P.Y > MaxY ? P.Y : MaxY;
					MinY = P.Y < MinY ? P.Y : MinY;
					MaxX = P.X > MaxX ? P.X : MaxX;
					MinX = P.X < MinX ? P.X : MinX;
				}
			
			correctParams();
			plot.Model = createPlotModel();
		}

		PlotModel createPlotModel() {
			var plot = new PlotModel {
				Background = OxyColor.Parse("#FFF0F0F0")
			};//{Title = title, Subtitle = ""};
			//y
			plot.Axes.Add(
				new LinearAxis {
					Position = AxisPosition.Left,
					Minimum = MinY,
					Maximum = MaxY,
					MajorStep = majorStepY,
					MinorStep = minorStepY,
					TickStyle = TickStyle.Inside
				}
			);
			//x
			plot.Axes.Add(
				new LinearAxis {
					Position = AxisPosition.Bottom,
					Minimum = MinX,
					Maximum = MaxX,
					MajorStep = majorStepX,
					MinorStep = minorStepX,
					TickStyle = TickStyle.Inside
				}
			);

			foreach (List<Point> pi in pLines) {
				LineSeries line = new LineSeries {
					LineStyle = LineStyle.None,
					MarkerSize = 3,
					MarkerType = MarkerType.Circle	
				};
				foreach (Point p in pi)
					line.Points.Add(new DataPoint(p.X, p.Y));
				plot.Series.Add(line);

				LineSeries pq = new LineSeries {
					LineStyle = LineStyle.None,
					MarkerSize = 10,
					MarkerType = MarkerType.Plus,
					MarkerStroke = OxyColor.Parse("#FFFD0000"),
					MarkerFill = OxyColor.Parse("#FFFD0000"),
					MarkerStrokeThickness = 5
				};
				pq.Points.Add(new DataPoint(pi[0].X, pi[0].Y));
				plot.Series.Add(pq);
			}
			return plot;
		}

		private void correctParams() {
			majorStepY = (int)(MaxY / 10);
			minorStepY = (int)(majorStepY / 10);

			majorStepX = (int)(MaxX / 10);
			minorStepX = (int)(majorStepX / 10);
		}
	}
}
