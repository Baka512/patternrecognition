﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace Распознавание_образов___4 {
	/// <summary>
	/// Логика взаимодействия для wPlot.xaml
	/// </summary>
	public partial class wPlotSmall : UserControl {
		private List<Point> pLine, centerLine;
		//List<Point> savePointsList;
		private double  MaxY = 0, MinY = 500000,
						MaxX = 0, MinX = 500000,
						majorStepX = 1.0, minorStepX = 1,
						majorStepY = 1.0, minorStepY = 0.1;
		public wPlotSmall() {
			InitializeComponent();
		}

		public void setData(/*float centrL, List<Point> PL*/int realPos, int teoreticalPos, List<List<Point>> elements) {
			//pLine = PL;

			//foreach(var P in pLine) {
			//	MaxY = P.Y > MaxY ? P.Y : MaxY;
			//	MinY = P.Y < MinY ? P.Y : MinY;
			//	MaxX = P.X > MaxX ? P.X : MaxX;
			//	MinX = P.X < MinX ? P.X : MinX;
			//}

			//centerLine = new List<Point>();
			//centerLine.Add(new Point(MinX, centrL));
			//centerLine.Add(new Point(MaxX, centrL));

			//correctParams();
			plot.Model = createPlotModel(realPos, teoreticalPos, elements);
		}

		PlotModel createPlotModel(int realPos, int teoreticalPos, List<List<Point>> elements) {
			OxyPlot.Wpf.PlotView plotMat = new OxyPlot.Wpf.PlotView();
			var plotModelMat = new PlotModel {
				Title = "",
				Subtitle = ""
			};

			//y
			plotModelMat.Axes.Add(new LinearAxis {
				Position = AxisPosition.Left,
			});

			//x
			plotModelMat.Axes.Add(new LinearAxis {
				Position = AxisPosition.Bottom,
			});

			var lineSeriesReal = new LineSeries {
				MarkerSize = 5,
				MarkerType = MarkerType.Circle,
				//Title = "Фактическое значение",

			};

			var lineSeriesTeotetical = new LineSeries {
				MarkerSize = 5,
				MarkerType = MarkerType.None,
				//Title = "Теор значение",

			};

			AreaSeries areaSeriesRange = new AreaSeries() {
				//Title = "5% коридор",
				Color =  OxyColor.FromArgb(255, 255, 0, 0),
				Color2 = OxyColor.FromArgb(255, 255, 0, 0),
				LineStyle = LineStyle.Automatic
			};

			foreach(List<System.Windows.Point> pi in elements) {
				Point pointReal = pi[realPos];
				Point pointTeoretical = pi[teoreticalPos];

				lineSeriesReal.Points.Add(new DataPoint(pointReal.X, pointReal.Y));
				lineSeriesTeotetical.Points.Add(new DataPoint(pointTeoretical.X, pointTeoretical.Y));

				 areaSeriesRange.Points.Add(new DataPoint(pointTeoretical.X, pointTeoretical.Y + pointTeoretical.Y * 0.05));
				areaSeriesRange.Points2.Add(new DataPoint(pointTeoretical.X, pointTeoretical.Y - pointTeoretical.Y * 0.05));
			}

			plotModelMat.Series.Add(areaSeriesRange);
			plotModelMat.Series.Add(lineSeriesReal);
			plotModelMat.Series.Add(lineSeriesTeotetical);

			return plotModelMat;
		}

		private void correctParams() {
			//majorStepY = (int) (MaxY / 10);
			//minorStepY = (int) (majorStepY / 10);

			//majorStepX = (int) (MaxX / 10);
			//minorStepX = (int) (majorStepX / 10);
			if(MinY == MaxY) {
				MinY--; MaxY++;
			}
			if(MinX == MaxX) {
				MinX--; MaxX++;
			}

			if(MinY <= 0)
				if(MaxY + Math.Abs(MinY) >= 80) {
					majorStepY = 10.0; minorStepY = 5.0;
				}
				else
					if(MaxY + Math.Abs(MinY) >= 30) {
						majorStepY = 5.0; minorStepY = 1.0;
					}
			if(MinY >= 0)
				if(MaxY - MinY >= 80) {
					majorStepY = 10.0; minorStepY = 5.0;
				}
				else
					if(MaxY - MinY >= 30) {
						majorStepY = 5.0; minorStepY = 1.0;
					}

			if(MaxX >= 10000) {
				majorStepX = (int) (MaxX / 10);
				minorStepX = (int) (majorStepX / 10);
			}
			else
				if(MaxX >= 5000) {
					majorStepX = 500.0; minorStepX = 250.0;
				}
				else
					if(MaxX >= 1000) {
						majorStepX = 200.0; minorStepX = 100.0;
					}
					else
						if(MaxX >= 400) {
							majorStepX = 50.0; minorStepX = 10.0;
						}
						else
							if(MaxX >= 250) {
								majorStepX = 10.0; minorStepX = 5.0;
							}
							else
								if(MaxX >= 50) {
									majorStepX = 5.0; minorStepX = 1.0;
								}
			if((MaxX <= 1) && (MaxX >= 0)) {
				majorStepX = 1; minorStepX = 0.5;
			}
		}
	}
}
