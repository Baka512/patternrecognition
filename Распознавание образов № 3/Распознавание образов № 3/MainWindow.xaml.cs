﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Распознавание_образов___3 {
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		List<TextBox> vectM = new List<TextBox>();
		List<TextBox> matrV = new List<TextBox>();
		
		List<int>	numList;
		List<int>	numVectCount;
		List<float>	pList;
		List<float[]>  matVectList; //Мат. ожидаине
		List<float[,]> corrMatrList; //корелляция
		int numOfShowClass = 1, numClassCount = 1;


		public MainWindow() {
			InitializeComponent();

			ClassNum.Text = numClassCount.ToString();
			ClassNum.Text = numClassCount.ToString();
			mBox.Header = "Класс  " + (numOfShowClass).ToString() + " из " + (numClassCount).ToString();

			getNewMV();
		}
		private void getNewMV() {
			numList      = new List<int>();
			numVectCount = new List<int>();
			pList        = new List<float>();
			matVectList  = new List<float[]>();
			corrMatrList = new List<float[,]>();

			int tempVecCount = 2;

			for(int k=0; k < numClassCount; k++) {
				float[]  tempMatV     = new float[tempVecCount];
				float[,] tempCorrMatr = new float[tempVecCount, tempVecCount];

				for(int i=0; i < tempVecCount; i++)
					tempMatV[i] = 200;

				for(int i=0; i < tempVecCount; i++)
					for(int j=0; j < tempVecCount; j++)
						tempCorrMatr[i, j] = (i == j) ? 50 : 0;
				numList.Add(300);
				numVectCount.Add(tempVecCount);
				pList.Add(1);
				matVectList.Add(tempMatV);
				corrMatrList.Add(tempCorrMatr);
			}
			drowMatr();
		}

		private void getNewMV(int tempVecCount = 2) {
			for(int k=0; k < numClassCount; k++) {
				float[]  tempMatV     = new float[tempVecCount];
				float[,] tempCorrMatr = new float[tempVecCount, tempVecCount];

				for(int i=0; i < tempVecCount; i++)
					tempMatV[i] = 200;

				for(int i=0; i < tempVecCount; i++)
					for(int j=0; j < tempVecCount; j++)
						tempCorrMatr[i, j] = (i == j) ? 50 : 0;
				numList     [numOfShowClass - 1] = 300;
				numVectCount[numOfShowClass - 1] = tempVecCount;
				pList       [numOfShowClass - 1] = 1;
				matVectList [numOfShowClass - 1] = tempMatV;
				corrMatrList[numOfShowClass - 1] = tempCorrMatr;
			}
			drowMatr();
		}

		private void drowMatr() {
			int			  tempNum =      numList[numOfShowClass - 1];
			int		 tempVecCount = numVectCount[numOfShowClass - 1];
			float         tempVer =        pList[numOfShowClass - 1];
			float[]      tempMatV =  matVectList[numOfShowClass - 1];
			float[,] tempCorrMatr = corrMatrList[numOfShowClass - 1];

			VecSize.Text = tempVecCount.ToString();
			elemNum.Text = tempNum.ToString();
			Ver.Text     = tempVer.ToString();

			vectM.Clear();
			matrV.Clear();
			matrGrid.Children.Clear();
			corGrid.Children.Clear();

			double x=20, y = 10;
			for(int i=0; i < tempVecCount; i++) {
				TextBox T = new TextBox();
				T.Text = tempMatV[i].ToString();
				matrGrid.Children.Add(T);
				T.VerticalAlignment = VerticalAlignment.Top; T.HorizontalAlignment = HorizontalAlignment.Left;
				T.Width = 40; T.Height = 25;
				T.TextAlignment = TextAlignment.Center;
				T.Margin = new Thickness(x, y, 0, 0);
				vectM.Add(T);
				x += 45;
			}

			x = 20; y = 10;
			for(int i=0; i < tempVecCount; i++) {

				for(int j=0; j <= i; j++) {
					TextBox T = new TextBox();
					T.Text = tempCorrMatr[i, j].ToString();
					corGrid.Children.Add(T);
					T.VerticalAlignment = VerticalAlignment.Top; T.HorizontalAlignment = HorizontalAlignment.Left;
					T.Padding = new Thickness(6);
					//T.FontSize = 12;
					T.Width = 40; T.Height = 40;

					T.TextAlignment = TextAlignment.Center;
					T.Margin = new Thickness(x, y, 0, 0);
					matrV.Add(T);
					x += 45;
				}
				x = 20;
				y += 45;
			}
			mBox.Header = "Класс  " + (numOfShowClass).ToString() + " из " + (numClassCount).ToString();
		}
		private bool saveData() {
			int	  tempNum;
			int   tempVecCount;
			float tempVer=0;
			float[]  tempMatV;
			float[,] tempCorrMatr;
			
			if(!Int32.TryParse(elemNum.Text, out tempNum))		 { MessageBox.Show("Введите корректное колличество элементов"); return false; }
			if(!Int32.TryParse(VecSize.Text, out tempVecCount))  { MessageBox.Show("Введите корректную размерность вектора");   return false; }
            if(!float.TryParse(Ver.Text ,    out tempVer))       { MessageBox.Show("Введите корректную вероятность"); return false; }

			    tempMatV = new float[tempVecCount];
			tempCorrMatr = new float[tempVecCount, tempVecCount];

			for(int i=0; i < tempVecCount; i++)
				if(!float.TryParse(vectM[i].Text, out tempMatV[i])) { MessageBox.Show("Введите корректное значение"); return false; }

			int k=0;
			for(int i=0; i < tempVecCount; i++)
				for(int j=0; j <= i; j++)
					if(!float.TryParse(matrV[k++].Text, out tempCorrMatr[i, j])) { MessageBox.Show("Введите корректное значение"); return false; }

			     numList[numOfShowClass - 1] = tempNum;
			numVectCount[numOfShowClass - 1] = tempVecCount;
			       pList[numOfShowClass - 1] = tempVer;
			 matVectList[numOfShowClass - 1] = tempMatV;
			corrMatrList[numOfShowClass - 1] = tempCorrMatr;

			return true;
		}
		private void Start_Button_Click(object sender, RoutedEventArgs e) {
			if(!saveData()) return;

			Random Rnd = new Random();
			List<List<Point>> pointLists = new List<List<Point>>();

			for(int k = 0; k < numClassCount; k++) {
				int			  tempNum =      numList[k];
				int		 tempVecCount = numVectCount[k];
				float         tempVer =        pList[k];
				float[]      tempMatV =  matVectList[k];
				float[,] tempCorrMatr = corrMatrList[k];

				List<Point> pointList = new List<Point>();

				// генерация выборки
				float [,] A=new float[tempVecCount, tempVecCount];
				A[0, 0] = tempCorrMatr[0, 0];
				for(int i = 0; i < tempVecCount; i++) {
					A[i, 0] = tempCorrMatr[i, 0];
					for(int j = 1; j < i; j++) {
						float sum = 0;
						for(int m = 0; m < i; m++)
							sum += A[i, m] * A[j, m];
						A[i, j] = (tempCorrMatr[i, j] - sum) / A[j, j];
					}
					float ssum = 0;
					for(int m = 0; m < i; m++)
						ssum += A[i, m] * A[i, m];
					A[i, i] = (float)Math.Sqrt(1 - ssum);

				}

				int Count = (int)(tempNum);
				float[,] Points = new float[Count, tempVecCount];
                pointList.Add(new Point(tempMatV[0] , tempMatV[1]));
				for(int i = 0; i < Count; i++) {
					float[] X = new float[tempVecCount];	//генерация некореллированной выборки
					for(int j = 0; j < tempVecCount; j++) {
						float sum = 0;
						for(int p = 0; p < 12; p++)
							sum += (float)Rnd.NextDouble();
						X[j] = sum - 6;
					}
					for(int j = 0; j < tempVecCount; j++) {
						Points[i, j] = 0;
						for(int  p = 0; p <= j; p++)
							Points[i, j] += X[p] * A[j, p];
						Points[i, j] = tempCorrMatr[j, j] * Points[i, j] + tempMatV[j];

					}
                    float genVer = (float)Rnd.NextDouble();
                    if(genVer <= tempVer)
					    pointList.Add(new Point(Points[i, 0], Points[i, 1]));
				}
				pointLists.Add(pointList);
			}
			wPlot.setData(pointLists);
		}
		private void pButP_Click(object sender, RoutedEventArgs e) {
			if(numClassCount > numOfShowClass)
				if(saveData()) {
					numOfShowClass++;
					drowMatr();
				}
		}
		private void pButM_Click(object sender, RoutedEventArgs e) {
			if(numOfShowClass > 1) 
				if(saveData()) {
					numOfShowClass--;
					drowMatr();
				}
		}
		private void ClassNumP_Click(object sender, RoutedEventArgs e) {
			if(numClassCount < 10) {
				numClassCount++;
				ClassNum.Text = numClassCount.ToString();
				getNewMV();
			}
		}
		private void ClassNumM_Click(object sender, RoutedEventArgs e) {
			if(numClassCount > 1) {
				numClassCount--;
				ClassNum.Text = numClassCount.ToString();
				getNewMV();
			}
		}
		private void VecSizeP_Click(object sender, RoutedEventArgs e) {
			int tempVecCount;
			Int32.TryParse(VecSize.Text, out tempVecCount);

			if(tempVecCount < 10) {
				tempVecCount++;
				VecSize.Text = tempVecCount.ToString();
				getNewMV(tempVecCount);
			}
		}
		private void VecSizeM_Click(object sender, RoutedEventArgs e) {
			int tempVecCount;
			Int32.TryParse(VecSize.Text, out tempVecCount);

			if(tempVecCount > 2) {
				tempVecCount--;
				VecSize.Text = tempVecCount.ToString();
				getNewMV(tempVecCount);
			}
		}
	}
}
